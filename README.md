# dotfiles-ubuntu
All relevant dotfiles of my kubuntu system
 
## Screenshots
![clean](screenshots/screenshot_1.png)
![busy](screenshots/screenshot_2.png)

## Wallpaper
![wallpaper](wallpaper.jpg)
This wallpaper has been created by Dominic Gut.
